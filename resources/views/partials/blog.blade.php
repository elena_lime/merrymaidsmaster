@extends('layout.app')
  @section('title')Blog | @endsection
  @section('description')Read {{ $data['allPages']->city }} blog to stay up to date about your cleaning services @endsection
  @section('content')
  <div class="container">
    <div class="pagesTopImage">
      <div class="imgBackground">
        <img src="{{ asset('/imgs/'.$meta->image) }}" alt="{{ $data['allPages']->city }} BLog" class="t" class="img-responsive"/>
        <div class="textSkew color1">
          <div class="text">
             {{ $meta->textOnImage }} 
          </div>
        </div>
      </div>
    </div>


  @include('layout.sidebar')
    <div class="clear hidden-lg hidden-md hidden-sm">
    </div>
    <div class="col-md-9 col-sm-8 col-xs-12 besideSideBar">
  @include('layout.breadcrumbs')
    <h1 class="blog-header">{{ $data['allPages']->city }}</h1>
     @foreach($body as $blogpost)
     <div class="post-container">
    @if($blogpost->image) 
      <img src="{{ asset('/imgs/'.$blogpost->image) }}" alt="{{ $blogpost->title }}" class="img-responsive blog-img"/> 
    @endif
    
      <a href="{{ asset('about-merry-maids/blog/'.$blogpost->slug) }}"><h3 class="post-title"> {{ $blogpost->title }}</h3></a>
      <p class="blog-date">{{ date("F-d-Y", strtotime($blogpost->date)) }}</p>
      <p>{{ $blogpost->briefDesc }}</p>
      <a href="{{ asset('about-merry-maids/blog/'.$blogpost->slug) }}" class="btn btn-primary">Read More</a>
      <hr>
    </div>
    @endforeach
  </div>
  </div>
  @include('layout.testimonials')
@endsection